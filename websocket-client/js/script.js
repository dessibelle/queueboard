/* Author:

*/


var wsUri = "ws://localhost:2004/";

//wsUri = "ws://echo.websocket.org";

var output;

function init() {
    output = document.getElementById("output");
    testWebSocket();
}

function testWebSocket() {
    
    console.log("testWebSocket");
    
    websocket = new WebSocket(wsUri);
    
    websocket.onopen = function(evt) {
        console.log("onopen");
        onOpen(evt)
    };
    websocket.onclose = function(evt) {
        console.log("onclose");
        onClose(evt)
    };
    websocket.onmessage = function(evt) {
        console.log("onmessage");
        onMessage(evt)
    };
    websocket.onerror = function(evt) {
        console.log("onerror");
        onError(evt)
    };
}

function onOpen(evt) {
    writeToScreen("CONNECTED");
    doSend("next");
}

function onClose(evt) {
    writeToScreen("DISCONNECTED");
}

function onMessage(evt) {
    writeToScreen('<span style="color: blue;">RESPONSE: ' + evt.data + '</span>');
}

function onError(evt) {
    writeToScreen('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function doSend(message) {
    writeToScreen("SENT: " + message);
    websocket.send(message);
}

function writeToScreen(message) {
    var pre = document.createElement("p");
    pre.style.wordWrap = "break-word";
    pre.innerHTML = message;
    output.appendChild(pre);
}

window.addEventListener("load", init, false);
