//
//  main.m
//  QueueBoard
//
//  Created by Simon Fransson on 2012-08-31.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc,  (const char **) argv);
}
