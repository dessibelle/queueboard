//
//  QueueBoardAppDelegate.h
//  QueueBoard
//
//  Created by Simon Fransson on 2012-08-30.
//  Copyright 2012 Hobo Code. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "QueueClient.h"

@class QueueView;

@interface QueueBoardAppDelegate : NSObject <QueueClientDelegate> {
    IBOutlet NSWindow *window;
    
    QueueView *itView;
    QueueView *attendantView;
	
	NSSound *alertSound;
}


- (void)initFullscreen;
- (void)initViews;
- (void)initQueue;


@end
