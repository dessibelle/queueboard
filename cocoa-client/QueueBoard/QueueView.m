//
//  QueueView.m
//  QueueBoard
//
//  Created by Simon Fransson on 2012-08-31.
//  Copyright 2012 Hobo Code. All rights reserved.
//

#import "QueueView.h"


@implementation QueueView

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        NSRect numberFrame = frame;
        NSRect titleFrame = frame;
        
        numberFrame.size.height = frame.size.height * 0.6;
        numberFrame.origin = NSMakePoint(0.0, frame.size.height * 0.2);
        
        titleFrame.origin = NSMakePoint(0.0, 0.0);
        titleFrame.size.height = frame.size.height * 0.1;
        
        _titleField = [[NSTextField alloc] initWithFrame:titleFrame];
        [_titleField retain];
        
        [_titleField setBezeled:NO];
        [_titleField setDrawsBackground:NO];
        [_titleField setEditable:NO];
        [_titleField setSelectable:NO];
        
        [_titleField setAlignment:NSCenterTextAlignment];
        
        [_titleField setFont:[NSFont fontWithName:@"Helvetica Neue" size:50]];
        
        [_titleField setStringValue:@""];
        
        [_titleField setAutoresizingMask:(NSViewMinXMargin | 
                                           NSViewWidthSizable |
                                           NSViewMaxXMargin |
                                           NSViewMinYMargin |
                                           NSViewHeightSizable |
                                           NSViewMaxYMargin)];
        
        
        
        
        
        _numberField = [[NSTextField alloc] initWithFrame:numberFrame];
        [_numberField retain];
        
        [_numberField setBezeled:NO];
        [_numberField setDrawsBackground:NO];
        [_numberField setEditable:NO];
        [_numberField setSelectable:NO];
        
        [_numberField setAlignment:NSCenterTextAlignment];
        
		/*
			LiquidCrystal-Regular
			SubwayTicker
			DS-Digital
			DisplayOTF
			Digital-7Mono
		*/
		
        [_numberField setFont:[NSFont fontWithName:@"Digital-7Mono" size:400]];
        
        [_numberField setStringValue:@""];
        
        [_numberField setAutoresizingMask:(NSViewMinXMargin | 
                                            NSViewWidthSizable |
                                            NSViewMaxXMargin |
                                            NSViewMinYMargin |
                                            NSViewHeightSizable |
                                            NSViewMaxYMargin)];
											
		[self setAutoresizesSubviews:YES];
        
    }
    return self;
}

- (void)viewDidMoveToSuperview
{
    [self addSubview:_numberField];
    [self addSubview:_titleField];
}

- (void)dealloc
{
    [_numberField release];
    [_titleField release];
    [super dealloc];
}

- (void)setNumber:(int)number
{
	NSString *numberString = [NSString stringWithFormat:@"%02d", number];

    [_numberField setStringValue:numberString];
}

- (void)setTitle:(NSString *)title
{
    [_titleField setStringValue:title];
}

/*
- (void)drawRect:(NSRect)dirtyRect {
    
    int red = rand() % 255;
    int green = rand() % 255;
    int blue = rand() % 255;
    NSColor* myColor = [NSColor colorWithCalibratedRed:red/255.0 green:green/255.0 blue:blue/255.0 alpha:1.0];
    
    
    [myColor setFill];
    NSRectFill(dirtyRect);
}         
*/

@end
