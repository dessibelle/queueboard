//
//  QueueBoardAppDelegate.m
//  QueueBoard
//
//  Created by Simon Fransson on 2012-08-30.
//  Copyright 2012 Hobo Code. All rights reserved.
//

#import "QueueBoardAppDelegate.h"
#import "QueueClient.h"
#import "QueueView.h"

typedef enum {
    QueueBoardQueueIT = 1,
    QueueBoardQueueAttendant
} QueueBoardQueue;

@implementation QueueBoardAppDelegate


- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {

	[self initFullscreen];
    [self initQueue];
    [self initViews];
    
	alertSound = [NSSound soundNamed:@"alert"];
}

- (void)applicationWillTerminate:(NSNotification *)notification
{
	[window orderOut:self];
        
	// Release the display(s)
	if (CGDisplayRelease( kCGDirectMainDisplay ) != kCGErrorSuccess) {
		NSLog( @"Couldn't release the display(s)!" );
		// Note: if you display an error dialog here, make sure you set
		// its window level to the same one as the shield window level,
		// or the user won't see anything.
	}
}

- (void)initFullscreen
{
int windowLevel;
    NSRect screenRect;
		
	// Capture the main display
    if (CGDisplayCapture( kCGDirectMainDisplay ) == kCGErrorSuccess) {
        
		// Get the shielding window level
		windowLevel = CGShieldingWindowLevel();
		
		// Get the screen rect of our main display
		screenRect = [[NSScreen mainScreen] frame];
		
		[window release];
		window = NULL;
		
		// Put up a new window
		window = [[NSWindow alloc] initWithContentRect:screenRect
                                styleMask:NSBorderlessWindowMask
                                backing:NSBackingStoreBuffered
                                defer:NO screen:[NSScreen mainScreen]];
								
		[window setLevel:windowLevel];
		
		[window setBackgroundColor:[NSColor whiteColor]];
		[window makeKeyAndOrderFront:nil];
		
		//[NSCursor setHiddenUntilMouseMoves:YES];	
		[NSCursor hide];
		
    } else {
		
		NSLog( @"Couldn't capture the main display!" );
	}
}

- (void)initViews
{	
	NSRect frame = [[window contentView] frame];
	
    frame.size.height = frame.size.height * 0.9;
    frame.origin.y = frame.size.height * 0.1;
    
	NSRect headingFrame = frame;
	headingFrame.size.height = frame.size.height * 0.2;
	
	headingFrame.origin.y = frame.size.height * 0.8;
	
	
	NSTextField *headingField = [[NSTextField alloc] initWithFrame:headingFrame];
	//[headingField retain];
        
	[headingField setBezeled:NO];
	[headingField setDrawsBackground:NO];
	[headingField setEditable:NO];
	[headingField setSelectable:NO];
	
	[headingField setAlignment:NSCenterTextAlignment];
	
	[headingField setFont:[NSFont fontWithName:@"Helvetica" size:50]];
	
	[headingField setStringValue:@"Just nu betj�nas"];
	
	[headingField setAutoresizingMask:(NSViewMinXMargin | 
                                           NSViewWidthSizable |
                                           NSViewMaxXMargin |
                                           NSViewMinYMargin |
                                           NSViewHeightSizable |
                                           NSViewMaxYMargin)];
	
	[[window contentView] addSubview:headingField];
	
	
    float new_width = frame.size.width / 2;
    
    NSRect it_frame = frame;
    it_frame.size.width = new_width;
    
    NSRect attandant_frame = it_frame;
    attandant_frame.origin.x = it_frame.origin.x + new_width;
    
    itView = [[QueueView alloc] initWithFrame:it_frame];
    [itView setTitle:NSLocalizedString(@"IT Support", @"IT label")];
    [[window contentView] addSubview:itView];
    
    [itView setAutoresizingMask:(NSViewMinXMargin | 
                                 NSViewWidthSizable |
                                 NSViewMaxXMargin |
                                 NSViewMinYMargin |
                                 NSViewHeightSizable |
                                 NSViewMaxYMargin)];
    
    attendantView = [[QueueView alloc] initWithFrame:attandant_frame];
    [attendantView setTitle:NSLocalizedString(@"Vaktm�steri", @"Attendant label")];
    [[window contentView] addSubview:attendantView];
    
    [attendantView setAutoresizingMask:(NSViewMinXMargin | 
                                 NSViewWidthSizable |
                                 NSViewMaxXMargin |
                                 NSViewMinYMargin |
                                 NSViewHeightSizable |
                                 NSViewMaxYMargin)];
								 
								 
	[[window contentView] setAutoresizingMask:(NSViewMinXMargin | 
									NSViewWidthSizable |
                                           NSViewMaxXMargin |
                                           NSViewMinYMargin |
                                           NSViewHeightSizable |
                                           NSViewMaxYMargin)];
										   
	[[window contentView] setAutoresizesSubviews:YES];
	
	[headingField release];
	[itView release];
	[attendantView release];
}

- (void)initQueue
{
    NSHost *host = [NSHost hostWithName:@"127.0.0.1"];
    int it_port = 2004;
    int attendant_port = 2005;
    
    QueueClient *itClient = [[QueueClient alloc] initWithHost:host port:it_port];
    QueueClient *attendantClient = [[QueueClient alloc] initWithHost:host port:attendant_port];
    
    //[itClient retain];
    [itClient setTag:QueueBoardQueueIT];
    [itClient setDelegate:self];
    [itClient connect];
    
    //[attendantClient retain];
    [attendantClient setTag:QueueBoardQueueAttendant];
    [attendantClient setDelegate:self];
    [attendantClient connect];
}


- (void)queueNumberChanged:(int)number client:(QueueClient *)client {
    
    switch ([client tag]) {
        case QueueBoardQueueIT:
            [itView setNumber:number];
		break;
        case QueueBoardQueueAttendant:
            [attendantView setNumber:number];
		break;
        default:
            break;
    }
	
	if (alertSound && ![alertSound isPlaying])
		[alertSound play];
}

@end
