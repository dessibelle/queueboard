//
//  QueueView.h
//  QueueBoard
//
//  Created by Simon Fransson on 2012-08-31.
//  Copyright 2012 Hobo Code. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface QueueView : NSView {
    
    NSTextField *_numberField;
    NSTextField *_titleField;
}

- (void)setNumber:(int)number;
- (void)setTitle:(NSString *)title;

@end
