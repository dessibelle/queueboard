package queueserver;

import java.io.*;
import java.net.*;


public class QueueServer
{
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = null;
        boolean listening = true;
        Queue queue = new Queue();
        
        int port = 2000;
        if (args.length > 0) {
            port = (int)(new Integer(args[0]));
        }
        
        try {
            System.out.println("Listening on port: " + new Integer(port).toString());
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            System.err.println("Could not listen on port: " + new Integer(port).toString());
            System.exit(-1);
        }
 
        while (listening) {
            new QueueServerThread(serverSocket.accept(), queue).start();
        }
        
        serverSocket.close();
    }
}
