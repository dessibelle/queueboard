package queueserver;

import java.util.Observable;

public class Queue extends Observable
{
    
    int number;
    int min;
    int max;
    
    Queue() {
        this.setMin(0);
        this.setMax(99);
        
        this.setNumber(this.min());
    }
    
    public int next() {
        
        this.number++;
        
        if (this.number > this.max) {
            this.number = this.min;
        }
        
        this.setChanged();
        this.notifyObservers();
        
        return this.number();
    }
    
    public int previous() {
        
        this.number--;
        
        if (this.number < this.min) {
            this.number = this.max;
        }
        
        this.setChanged();
        this.notifyObservers();
        
        return this.number();
    }
    
    public int number() {
        return this.number;
    }
    
    public int min() {
        return this.min;
    }
    
    public int max() {
        return this.max;
    }
    
    public void setNumber(int _number) {
        this.number = _number;
    }
    
    public void setMin(int _number) {
        this.min = _number;
    }
    
    public void setMax(int _number) {
        this.max = _number;
    }
}