package queueserver;

import java.io.*;
import java.net.*;
import java.util.concurrent.*;
import java.util.Observable;
import java.util.Observer;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;

//import org.apache.commons.codec.binary.Base64;
import sun.misc.BASE64Encoder;

public class QueueServerThread extends Thread implements Observer {
    protected Socket socket = null;
    protected Queue queue;
    
    protected PrintWriter out = null;
    protected BufferedReader in = null;
    
    protected boolean use_websocket = false;
    protected boolean handshaken = false;
    protected String handshake = "";
    protected String secWebSocketKey = "";
    
    public QueueServerThread(Socket socket, Queue queue) {
        super("QueueServerThread");
        this.socket = socket;
        
        System.out.println("Connection received from " + this.socket.getInetAddress().getHostName());
        
        this.queue = queue;        
        this.queue.addObserver(this);
    }
 
    public void run() {
 
        try {
            this.out = new PrintWriter(this.socket.getOutputStream(), true);
            this.in = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
 
            String inputLine, outputLine;
 
            while ((inputLine = this.in.readLine()) != null) {
                outputLine = "unknown command";

                //System.out.println("command: " + inputLine);

                if (inputLine.equals("next") || inputLine.equals("increase"))
                {
                    outputLine = new Integer(this.queue.next()).toString();
                }
                else if (inputLine.equals("previous") || inputLine.equals("decrease") || inputLine.equals("prev"))
                {
                    outputLine = new Integer(this.queue.previous()).toString();
                }
                else if (inputLine.equals("current") || inputLine.equals("number") || inputLine.equals("num"))
                {
                    outputLine = new Integer(this.queue.number()).toString();
                    sendMessage(outputLine);
                }
                else if (inputLine.equals("min") || inputLine.equals("minimum"))
                {
                    outputLine = new Integer(this.queue.min()).toString();
                }
                else if (inputLine.equals("max") || inputLine.equals("maximum"))
                {
                    outputLine = new Integer(this.queue.max()).toString();
                }
                else if (inputLine.equals("quit") || inputLine.equals("exit"))
                {
                    outputLine = "QUIT";
                }
                else if (this.use_websocket && !this.handshaken)
                {
                    if (inputLine.equals("")) {
                        System.out.println("# sending handshake");

                        String hs = this.handshake();
                        this.out.println(hs);

                        System.out.println("# handshake sent");
                    } else {
                        if (inputLine.toLowerCase().startsWith("sec-websocket-key:")) {
                            String[] parts = inputLine.split(":");
                            
                            this.secWebSocketKey = parts[1].trim();
                            
                            System.out.println(this.secWebSocketKey);
                        }
                    }
                }

                
                
                if (outputLine.equals("QUIT")) {
                    System.out.println("Client " + this.socket.getInetAddress().getHostName() + " disconnected");
                    break;
                } else {
                    System.out.println("# command: '" + inputLine + "' => " + outputLine);
                }
            }
            
            this.out.close();
            this.in.close();
            this.socket.close();
 
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    protected String handshake() {
        
        this.handshake = "HTTP/1.1 101 Web Socket Protocol Handshake\r\n" + 
        "Upgrade: WebSocket\r\n" + 
        "Connection: Upgrade\r\n" + 
        "WebSocket-Origin: http://" + this.socket.getInetAddress().getHostName() + ":" + this.socket.getPort() + "\r\n" +
        "WebSocket-Location: ws://" + this.socket.getLocalAddress().getHostName() + ":" + this.socket.getLocalPort() + "/\r\n" + 
        "Sec-WebSocket-Accept: " + this.secWebSocketAccept() + "\r\n" +
        
        "\r\n";
        
        return this.handshake;
    }
    
    protected String secWebSocketAccept() {
        /*
        The client sends a Sec-WebSocket-Key which is base64 encoded.
        To form a response, the magic string 258EAFA5-E914-47DA-95CA-C5AB0DC85B11 is appended to this (undecoded) key.
        The resulting string is then hashed with SHA-1, then base64 encoded.
        Finally, the resulting reply occurs in the header Sec-WebSocket-Accept.
        */

        String accept;
        String magic = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";

        accept = this.secWebSocketKey + magic;      //System.out.println("Accept Magic: " + accept);
        accept = new String(this.SHA1bytes(accept.getBytes())); //System.out.println("SHA1 #1: " + accept);        
        
        BASE64Encoder encoder = new BASE64Encoder();
        accept = encoder.encodeBuffer(accept.getBytes());   //System.out.println("Base 64 #1: " + accept);
        
        return accept;
    }
    
    protected void sendMessage(String m) {
        if (this.use_websocket) {
            this.out.println((char)0x00 + m + (char)0xff);
        } else {
            this.out.println(m);
        }
        
    }
    
    public void update(Observable o, Object arg) {
        this.sendMessage(new Integer(this.queue.number()).toString());        
    }
    
    
    public static byte[] SHA1bytes(byte[] convertme) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-1");
        }
        catch(NoSuchAlgorithmException e) {
            e.printStackTrace();
        } 
        return md.digest(convertme);
    }
    
    public static String toSHA1(byte[] convertme) {
        return byteArrayToHexString(SHA1bytes(convertme));
    }
    
    public static String byteArrayToHexString(byte[] b) {
      String result = "";
      for (int i=0; i < b.length; i++) {
        result +=
              Integer.toString( ( b[i] & 0xff ) + 0x100, 16).substring( 1 );
      }
      return result;
    }
    
}
