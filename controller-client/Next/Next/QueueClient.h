//
//  QueueClient.h
//  QueueBoard
//
//  Created by Simon Fransson on 2012-08-31.
//  Copyright 2012 Hobo Code. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface QueueClient : NSObject <NSStreamDelegate> {

    NSHost          *_host;
    int				_port;
    
    int				_number;
    
    NSInputStream   *_inputStream;
    NSOutputStream  *_outputStream;
    
    NSMutableData   *_data;
    int				bytesRead;
    
    int				tag;
    
    BOOL            _open_complete;
    BOOL            _handshaken;
    
    id delegate;
}


- (id)initWithHost:(NSHost *)host port:(int)port;
- (void)connect;
- (void)disconnect;

- (void)setTag:(int)aTag;
- (int)tag;

- (id)delegate;
- (void)setDelegate:(id)aDelegate;

- (void)sendCommand:(NSString *)command;



@end


@protocol QueueClientDelegate 

- (void)queueNumberChanged:(int)number client:(QueueClient *)client;

@end