//
//  main.m
//  Next
//
//  Created by Simon Fransson on 2012-09-01.
//  Copyright (c) 2012 Simon Fransson. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
