//
//  SFAppDelegate.m
//  Next
//
//  Created by Simon Fransson on 2012-09-01.
//  Copyright (c) 2012 Simon Fransson. All rights reserved.
//

#import "SFAppDelegate.h"
#import "QueueClient.h"

@implementation SFAppDelegate

@synthesize client = _client,
numberView = _numberView,
nextButton = _nextButton,
prevButton = _prevButton;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    [[NSUserDefaults standardUserDefaults] registerDefaults:[NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Defaults" ofType:@"plist"]]];
    
    [self setupNotifications];
    
    [self initViews];
    [self initQueue];
    
    [self.window setMinSize:NSMakeSize(315, 400)];
}


- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)theApplication
{
    return YES;
}

- (void)applicationWillTerminate:(NSNotification *)notification
{
    [self.client sendCommand:@"exit"];
    
    [[NSApplication sharedApplication] terminate:self];
}

- (void)connect
{
    if (self.client) {
        [self.client disconnect];
    }
    
    self.client = NULL;
    
    [self initQueue];
}


- (void)setupNotifications
{
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self
               selector:@selector(defaultsChanged:)
                   name:NSUserDefaultsDidChangeNotification
                 object:nil];
    
    [[[NSWorkspace sharedWorkspace] notificationCenter] addObserver: self
                                                           selector: @selector(receiveSleepNote:)
                                                               name: NSWorkspaceWillSleepNotification object: NULL];
    
    [[[NSWorkspace sharedWorkspace] notificationCenter] addObserver: self
                                                           selector: @selector(receiveWakeNote:)
                                                               name: NSWorkspaceDidWakeNotification object: NULL];
}

- (void)initViews
{
	NSRect frame = [[self.window contentView] frame];
	
    frame.size.height = frame.size.height * 0.85;
    frame.origin.y = frame.size.height * 0.2;
    
    
    self.numberView = [[NSTextField alloc] initWithFrame:frame];
    [[self.window contentView] addSubview:self.numberView];
    
    [self.numberView setBezeled:NO];
    [self.numberView setDrawsBackground:NO];
    [self.numberView setEditable:NO];
    [self.numberView setSelectable:NO];
    
    [self.numberView setAlignment:NSCenterTextAlignment];
    
    
    CFErrorRef error;
    
    NSString *bundleIdentifier = [[[NSBundle bundleForClass:[self class]] infoDictionary] objectForKey:@"CFBundleIdentifier"];

    NSURL *fontURL = [[[[NSBundle bundleWithIdentifier:bundleIdentifier] resourceURL] URLByAppendingPathComponent:@"digital-7 (mono)"] URLByAppendingPathExtension:@"ttf"];
    BOOL fontActivated = CTFontManagerRegisterFontsForURL((CFURLRef)CFBridgingRetain(fontURL), kCTFontManagerScopeProcess, &error);
    
    
    NSFont *font;
    
    if (fontActivated) {
        font = [NSFont fontWithName:@"Digital-7Mono" size:300];
    } else {
        font = [NSFont fontWithName:@"Helvetica" size:300];
    }
    

    [self.numberView setFont:font];
    [self.numberView setStringValue:@""];

    
    [self.numberView setAutoresizingMask:(NSViewMinXMargin |
                                 NSViewWidthSizable |
                                 NSViewMaxXMargin |
                                 NSViewMinYMargin |
                                 NSViewHeightSizable |
                                 NSViewMaxYMargin)];
    
    
    
}

- (void)defaultsChanged:(NSNotification *)notification {

    NSLog(@"defaultsChanged");
    
    [self connect];
}

- (void)initQueue
{
    NSHost *host = [NSHost hostWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"Host"]];
    int port = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"Port"];
    
    self.client = [[QueueClient alloc] initWithHost:host port:port];
    
    [self.client setDelegate:self];
    [self.client connect];
}


- (void)queueNumberChanged:(int)number client:(QueueClient *)client {
    
	NSString *numberString = [NSString stringWithFormat:@"%02d", number];
    [self.numberView setStringValue:numberString];
}



- (IBAction)next:(id)sender {
    [self.client sendCommand:@"next"];
}

- (IBAction)prev:(id)sender {
    [self.client sendCommand:@"previous"];
}

- (IBAction)reconnect:(id)sender {
    
    [self connect];
}


- (void)receiveSleepNote: (NSNotification*) note
{
    NSLog(@"receiveSleepNote: %@", [note name]);
    
    [self.client disconnect];
}

- (void)receiveWakeNote: (NSNotification*) note
{
    NSLog(@"receiveSleepNote: %@", [note name]);
    
    [self connect];
}




@end
