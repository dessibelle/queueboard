//
//  SFAppDelegate.h
//  Next
//
//  Created by Simon Fransson on 2012-09-01.
//  Copyright (c) 2012 Simon Fransson. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class QueueClient;

@interface SFAppDelegate : NSObject <NSApplicationDelegate> {
    
    QueueClient *_client;
    
    NSTextField *_numberView;
    
    __unsafe_unretained NSButton *_nextButton;
    __unsafe_unretained NSButton *_prevButton;
}

@property (assign) IBOutlet NSWindow *window;
@property (nonatomic, assign) IBOutlet NSButton *nextButton;
@property (nonatomic, assign) IBOutlet NSButton *prevButton;

@property (retain) QueueClient *client;
@property (retain, nonatomic) NSTextField *numberView;

- (void)initQueue;
- (void)initViews;
- (void)setupNotifications;
- (void)defaultsChanged:(NSNotification *)notification;
- (void)receiveSleepNote:(NSNotification *)note;
- (void)receiveWakeNote:(NSNotification *)note;
- (void)queueNumberChanged:(int)number client:(QueueClient *)client;


- (IBAction)next:(id)sender;
- (IBAction)prev:(id)sender;
- (IBAction)reconnect:(id)sender;

@end
