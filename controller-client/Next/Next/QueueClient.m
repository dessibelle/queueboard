//
//  QueueClient.m
//  QueueBoard
//
//  Created by Simon Fransson on 2012-08-31.
//  Copyright 2012 Hobo Code. All rights reserved.
//

#import "QueueClient.h"


@implementation QueueClient


- (id)initWithHost:(NSHost *)host port:(int)port
{
    if (self = [super init]) {
        _port = port;
        _host = host;
        _number = 0;
        
        _open_complete = NO;
        _handshaken = NO;
    }
    return self;
}

- (void)connect
{
    if (_host) {
        bytesRead = 0; 
        
        [NSStream getStreamsToHost:_host port:_port inputStream:&_inputStream outputStream:&_outputStream];
        
        [_inputStream retain];
        [_outputStream retain];
        
        [_inputStream setDelegate:self];
        [_outputStream setDelegate:self];
        
        [_inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop]
                           forMode:NSDefaultRunLoopMode];
        [_outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop]
                           forMode:NSDefaultRunLoopMode];
        [_inputStream open];
        [_outputStream open];
    }
}

- (void)disconnect
{
    [self sendCommand:@"exit"];
    
    [_inputStream setDelegate:nil];
    [_outputStream setDelegate:nil];
    
    [_inputStream removeFromRunLoop:[NSRunLoop currentRunLoop]
                            forMode:NSDefaultRunLoopMode];
    [_outputStream removeFromRunLoop:[NSRunLoop currentRunLoop]
                             forMode:NSDefaultRunLoopMode];
    
    [_inputStream close];
    [_outputStream close];
    
    [_inputStream release];
    [_outputStream release];
    
    _inputStream = nil;
    _outputStream = nil;
}

- (void)stream:(NSStream *)stream handleEvent:(NSStreamEvent)eventCode {
    
    switch(eventCode) {
        case NSStreamEventNone:
        {
            NSLog(@"NSStreamEventNone");
            
            break;
        }
        case NSStreamEventOpenCompleted:
        {
            NSLog(@"NSStreamEventOpenCompleted");
            
            _open_complete = YES;
            
            break;
        }
        case NSStreamEventHasBytesAvailable:
        {
            NSLog(@"NSStreamEventHasBytesAvailable");
            
            if(!_data) {
                _data = [[NSMutableData data] retain];
            }
            uint8_t buf[1024];
            unsigned int len = 0;
            len = (unsigned int)[(NSInputStream *)stream read:buf maxLength:1024];
            
            if(len) {
                [_data appendBytes:(const void *)buf length:len];

                bytesRead = bytesRead + len;
                
                NSString *inputValue = [[NSString alloc] initWithData:_data encoding:NSUTF8StringEncoding];
                _number = [inputValue intValue];
                
                if (delegate) {
                    [delegate queueNumberChanged:_number client:self];
                }
                
                [inputValue release];
                
                bytesRead = 0;
                [_data release];
                _data = NULL;
                
            } else {
                NSLog(@"no buffer!");
            }
            break;
        }
        case NSStreamEventHasSpaceAvailable:
        {
            NSLog(@"NSStreamEventHasSpaceAvailable");
            
            if (_open_complete)
            {
                if (!_handshaken)
                {
                    [self sendCommand:@"number"];
                    _handshaken = YES;
                }
            }
            
            break;
            
        }
        case NSStreamEventErrorOccurred:
        {
            NSLog(@"NSStreamEventErrorOccurred");
            
            break;
        }
        case NSStreamEventEndEncountered:
        {
            NSLog(@"NSStreamEventEndEncountered");
            
            [stream close];
            [stream removeFromRunLoop:[NSRunLoop currentRunLoop]
                              forMode:NSDefaultRunLoopMode];
            [stream release];
            stream = nil; // stream is ivar, so reinit it
            break;
        }            
    }
}

- (void)setTag:(int)aTag
{
    tag = aTag;
}

- (int)tag
{
    return tag;
}


- (id)delegate {
    return delegate;
}

- (void)setDelegate:(id)aDelegate {
    delegate = aDelegate;
}

- (void)sendCommand:(NSString *)command
{
    //[_commands addObject:command];
    
    if (command && _outputStream) {
        
        command = [command stringByAppendingString:@"\r\n"];
        
        NSData *data = [command dataUsingEncoding:NSUTF8StringEncoding];
        unsigned int len = (unsigned int)[data length];
        
        [_outputStream write:[data bytes] maxLength:len];
    }
}

@end
