//
//  NextTests.m
//  NextTests
//
//  Created by Simon Fransson on 2012-09-01.
//  Copyright (c) 2012 Simon Fransson. All rights reserved.
//

#import "NextTests.h"

@implementation NextTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in NextTests");
}

@end
